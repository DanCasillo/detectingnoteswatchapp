//
//  InterfaceController.swift
//  DetectingNotesWatchApp WatchKit Extension
//
//  Created by Danilo Casillo on 15/01/2020.
//  Copyright © 2020 Danilo Casillo. All rights reserved.
//

import WatchKit
import Foundation
import CoreML
import SoundAnalysis
import AVFoundation

class InterfaceController: WKInterfaceController {

    //Notes and Model definitions
    var notes : GuitarNotes!
    var model : MLModel!
    
    //File analyzer
    var audioFileAnalyzer : SNAudioFileAnalyzer!
    //Analysis observer
    var observer : AnalysisObserver!

    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        
    }
    
    
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        notes = GuitarNotes()
        model = notes.model
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func startButtonTapped() {
        //Change audioFileURL with a mp3 files...
        
        //Bijoux is the A2 - PIANO NORMAL.mp3 FILE
        let audioFileURL = URL(fileURLWithPath: "/Users/danilocasillo/Desktop/Bijoux.mp3") // URL of audio file to analyze (m4a, wav, mp3, etc.)
                    
        // Create a new audio file analyzer.
        do {
            
        audioFileAnalyzer = try SNAudioFileAnalyzer(url: audioFileURL)
        observer = AnalysisObserver()

        // Prepare a new request for the trained model.
        let request = try SNClassifySoundRequest(mlModel: model)
        try audioFileAnalyzer.add(request, withObserver: observer)
        
            audioFileAnalyzer.analyze()
        }
        catch {
            print(error)
        }
    }
    
}
